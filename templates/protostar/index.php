<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Output as HTML5
$doc->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/template.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/custom.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/dist/css/bootstrap.min.css');
//$doc->addStyleSheet("//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css");

// Use of Google Font
if ($this->params->get('googleFont'))
{
	$doc->addStyleSheet('//fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
	$doc->addStyleDeclaration("
	h1, h2, h3, h4, h5, h6, .site-title {
		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
	}");
}

// Template color
if ($this->params->get('templateColor'))
{
	$doc->addStyleDeclaration("
	body.site {
		border-top: 3px solid " . $this->params->get('templateColor') . ";
		background-color: " . $this->params->get('templateBackgroundColor') . ";
	}
	a {
		color: " . $this->params->get('templateColor') . ";
	}
	.nav-list > .active > a,
	.nav-list > .active > a:hover,
	.dropdown-menu li > a:hover,
	.dropdown-menu .active > a,
	.dropdown-menu .active > a:hover,
	.nav-pills > .active > a,
	.nav-pills > .active > a:hover,
	.btn-primary {
		background: " . $this->params->get('templateColor') . ";
	}");
}

// Check for a custom CSS file
$userCss = JPATH_SITE . '/templates/' . $this->template . '/css/user.css';

if (file_exists($userCss) && filesize($userCss) > 0)
{
	$this->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/user.css');
}

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<!--[if lt IE 9]><script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script><![endif]-->
</head>
<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
	echo ($this->direction == 'rtl' ? ' rtl' : '');
?>">
	<!-- Body -->
	<div class="body">
		<div class="row-fluid">
			<!-- Header -->
			<?/*
			<header class="header" role="banner">
				<div class="header-inner clearfix">
					<a class="brand pull-left" href="<?php echo $this->baseurl; ?>/">
						<?php echo $logo; ?>
						<?php if ($this->params->get('sitedescription')) : ?>
							<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
						<?php endif; ?>
					</a>
					<div class="header-search pull-right">
						<jdoc:include type="modules" name="position-0" style="none" />
					</div>
				</div>
			</header>*/?>

			<?php/* if ($this->countModules('position-1')) : ?>
				<nav class="navigation" role="navigation">
					<div class="navbar pull-left">
						<a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
					</div>
					<div class="nav-collapse">
						<jdoc:include type="modules" name="position-1" style="none" />
					</div>
				</nav>
			<?php endif; */?>
			<div global-header="" class="glorious-header theme-inverse-overlay GlobalHeader" data-section="header">
					<div class="wrapperx">
						<div class="row header-row">
							<div class="header-logo pull-left">
								<a href="."><img class="GlobalHeader-fallbackLogo" src="images/kliq-logo.png" alt="Kliq" height="24" width="152"></a>
							</div>
							<div class="header-middle-container">
							</div>
							<div class="header-navigation  pull-right">
								<a href="#" rel="nofollow" aura-track="home page/clicked join as a pro" aura-track-on="click" class="gray-link">Join as a Pro</a>
								<a href="#" rel="nofollow" aura-track="home page/clicked sign up" aura-track-on="click" class="gray-link">Sign Up</a>
								<a href="#" rel="nofollow" aura-track="home page/clicked log in" aura-track-on="click" class="gray-link log-in-link">Log In</a>
							</div>
						</div>
					</div>
				</div>

				<div class="Hero HeroSection ng-scope" ng-controller="TrackingController">
		    <div class="ContentSection">
		        <h1>Your business, your way.</h1>
		        <p>Choose your customers. Quote your price. Keep what you earn.</p>
		        <div class="ProfessionSelector-container">
		            <div class="ProfessionSelector ng-scope" ng-controller="ProfessionController">
		                <form ng-submit="navigateToWelcomeSuggestedServices($event)" lpformnum="2" class="ng-pristine ng-invalid ng-invalid-required">
		                        <div class="form-field form-field-category">
		                            <span class="input-wrapper">
		                                <span search-form="" hercule-root-url="https://hercule.thumbtack.io" hercule-version="" hercule-limit="15" more-results-hercule="true" class="SearchForm ng-scope">
		                                    <input class="query ng-pristine ng-invalid ng-invalid-required" required="" autocomplete="off" placeholder="What service do you provide?" ng-model="$parent.$selection" aura-track="service signup/start service query" aura-track-on="keypress" aura-track-once="" type="text">
		                                </span>
		                            </span>
		                        </div>
		                        <div class="form-field-nav">
		                            <button class="bttn" type="submit" data-sticky-partner="">
		                                Get Started
		                            </button>
		                        </div>
		                    </form>
		                            </div>
		        </div>
		    </div>
		</div>

		<div class="quick_boxes" style="background: #f4f4f4;">
			<div class="container hiddenx">
				<div class="col-xs-6 col-md-2 center-block">
					<a href="#"  class="icon-events"></a>
					<a href="#">Events</a>
				</div>
				<div class="col-xs-6 col-md-2 center-block">
					<a href="#"  class="icon-homex"></a>
				<a href="#">	Home</a>
				</div>
				<div class="col-xs-6 col-md-2 center-block">
					<a href="#" class="icon-lessons"></a>
					<a href="#">Lessons</a>
				</div>
				<div class="col-xs-6 col-md-2 center-block">
					<a href="#"  class="icon-wellness"></a>
					<a href="#">Wellness</a>
				</div>
				<div class="col-xs-6 col-md-2 center-block">
					<a href="#"  class="icon-business"></a>
					<a href="#">Business</a>
				</div>
				<div class="col-xs-6 col-md-2 center-block">
					<a href="#"  class="icon-more"></a>
					<a href="#">More</a>
				</div>
			</div>
			<nav class="CategoryNav hidden">
				<ul class="CategoryNav-list">
					<li class="CategoryNav-item">
						<a class="icon-user hasTooltip"></a>
						<span class="T2-S CategoryNav-item-label">icon</span>
					</li>
					<li class="CategoryNav-item">
						<a class="icon-user hasTooltip"></a>
						<span class="T2-S CategoryNav-item-label">icon</span>
					</li>
				</ul>
			</nav>
		</div>
			<jdoc:include type="modules" name="banner" style="xhtml" />
			<div class="mainContent">
				<div class="page-grid">
				<?php if ($this->countModules('position-8')) : ?>
					<!-- Begin Sidebar -->
					<div id="sidebar" class="span3">
						<div class="sidebar-nav">
							<jdoc:include type="modules" name="position-8" style="xhtml" />
						</div>
					</div>
					<!-- End Sidebar -->
				<?php endif; ?>
				<main id="content" role="main" class="<?php echo $span; ?>">
					<!-- Begin Content -->
					<jdoc:include type="modules" name="position-3" style="xhtml" />
					<jdoc:include type="modules" name="position-2" style="none" />
					<jdoc:include type="message" />
					<jdoc:include type="component" />
					<!-- End Content -->
				</main>
				<?php if ($this->countModules('position-7')) : ?>
					<div id="aside" class="span3">
						<!-- Begin Right Sidebar -->
						<jdoc:include type="modules" name="position-7" style="well" />
						<!-- End Right Sidebar -->
					</div>
				<?php endif; ?>
			</div><br />
		</div>
		</div>
	</div>
	<!-- Footer -->
	<?/*<footer class="footer" role="contentinfo">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<hr />
			<jdoc:include type="modules" name="footer" style="none" />
			<p class="pull-right">
				<a href="#" id="back-top">
					<?php echo JText::_('TPL_PROTOSTAR_BACKTOTOP'); ?>
				</a>
			</p>
			<p>
				&copy; <?php echo date('Y'); ?> <?php echo $sitename; ?>
			</p>
		</div>
	</footer>*/?>
	<div class="row-fluid how-it-works">
		<jdoc:include type="modules" name="how-kliq" style="none" />
	</div>
	<div class="GlobalFooter piede">
    <div class="wrapper">
            <div class="dynamic-row navigation page-grid">
                <div class="column-4">
                    <div class="copyright">
                        <a href="index.php" class="GlobalFooter-header-logo logo">
                        	<img src="images/kliq-logo.png" alt="Kliq">
                        </a>

                        <div class="social">
													<ul class="social-media">
    												<li>
											        <a href="https://www.facebook.com/Thumbtack" target="_blank">
											            <span class="facebook"></span>
											        </a>
    												</li>
    												<li>
											        <a href="https://plus.google.com/110365850422119512162/posts" target="_blank">
											            <span class="gplus"></span>
											        </a>
												    </li>
												    <li>
												        <a href="http://twitter.com/thumbtack" target="_blank">
												            <span class="twitter"></span>
												        </a>
												    </li>
												    <li>
												        <a href="https://www.thumbtack.com/blog/" target="_blank">
												            <span class="blog"></span>
												        </a>
												    </li>
												    <li>
												        <a href="http://pinterest.com/thumbtackboards/" target="_blank">
												            <span class="pinterest"></span>
												        </a>
												    </li>
												</ul>
												</div>
        						</div>
                </div>

                <div class="column-2">
                    <h4 class="GlobalFooter-linkSection-header T2-S">Company</h4>
										<jdoc:include type="modules" name="footer-company-menu" style="none" />
                </div>
                <div class="column-2">
                    <h4 class="GlobalFooter-linkSection-header T2-S">Customers</h4>
										<jdoc:include type="modules" name="footer-customers-menu" style="none" />
                </div>
                <div class="column-2">
                    <h4 class="GlobalFooter-linkSection-header T2-S">Pros</h4>
										<jdoc:include type="modules" name="footer-pros-menu" style="none" />
                </div>
                <div class="column-2">
                    <h4 class="GlobalFooter-linkSection-header T2-S">
                        Questions?
                        <span>Need help?</span>
                    </h4>
										<jdoc:include type="modules" name="footer-question-menu" style="none" />
                </div>
            </div>
            </div>
</div>

<div class="dynamic-row GlobalFooter-legal">
		<div class="page-grid	">
				<div class="">
						<a href="." class="GlobalFooter-legal-copyright B3-S pull-left">© Kliq, Inc.</a>
							<div class="GlobalFooter-legal-disclosures B3-S pull-right">
								<a href="privacy" target="_blank">Privacy policy</a> | <a href="terms" target="_blank" class="last">Terms of use</a>
						</div>
				</div>
		</div>
</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
